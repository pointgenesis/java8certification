/**
 * Copyright (c) 2016 Point Genesis Solutions LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.pointgenesis.tutorials.utilities;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class NumberUtilsTest {

	@Test
		public void testGetRandomNumberInRange() {
			int min = 5;
			int max = 10;
			
			//CASE-1: Normal (POSITIVE TEST)
			int value = NumberUtils.getRandomNumberInRange(min, max);
			assertTrue("The returned number is less than or equal to the max value, and greater than or equal to the min value.", (value >= min) && (value <= max));
			
			//CASE-2: min < max (NEGATIVE TEST)
			value = -1;
			min = 10;
			max = 5;
			try {
				value = NumberUtils.getRandomNumberInRange(min,  max);
				fail("This should have thrown an IllegalArgumentException.");
			} catch (IllegalArgumentException e) {
				assertTrue("The method invocation was expected to throw and IllegalArgumentException.", value == -1);
			}
			
			//CASE-3: negative values (POSITIVE TEST)
			value = -1;
			min = -10;
			max = 5;
			try {
				value = NumberUtils.getRandomNumberInRange(min,  max);
				assertTrue((value >= min) && (value <= max));
			} catch (IllegalArgumentException e) {
				fail("This should NOT have thrown an IllegalArgumentException.");
			}
			assertTrue((value >= min) && (value <= max));
			
		}
	
	@Test
	public void testIsOdd() throws Exception {
		for (int i = 0; i < 100; i++) {
			int value = NumberUtils.getRandomNumberInRange(0, 1000);
			boolean isValueOdd = NumberUtils.isOdd(value);
			
			int remainder = value%2;
			if (remainder == 0) {
				assertTrue("This is an even number.", isValueOdd == false);
			} else {
				assertTrue("This is an odd number.", isValueOdd == true);
			}
		}
	}
}
