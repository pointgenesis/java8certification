/**
 * Copyright (c) 2016 Point Genesis Solutions LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.pointgenesis.tutorials.utilities;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class NumberUtils {
	private static final Logger logger = LogManager.getLogger();
	/**
	 * Determines if a <code>int</code> value is EVEN or ODD.
	 * @param value The numeric value to access.
	 * @return true if the value is ODD; otherwise, false (value is EVEN).
	 */
	public static boolean isOdd(int value) {
		String bits = Integer.toBinaryString(value);
		logger.debug("value: {} bits: {}", value, bits);
		if (bits.charAt(bits.length()-1) == '1') {
			logger.debug("value: {} is ODD.", value);
			return true;
		}
		logger.debug("value: {} is EVEN.", value);
		return false;
	}
	
	/**
	 * Returns a random number [lowerBound, upperBound] inclusive of the end points.
	 * @param lowerBound The lower end point.
	 * @param upperBound The upper end point.
	 * @return A random number between the given lower and upper boundary points. The end points are inclusive, meaning that they can be returned as a random value.
	 * @throws IllegalArgumentException if the lowerBound is less than or equal to the upperBound.
	 */
	public static int getRandomNumberInRange(int lowerBound, int upperBound) throws IllegalArgumentException {
		logger.debug("lowerBound: {}  upperBound: {}", lowerBound, upperBound);
		if (lowerBound >= upperBound) {
			throw new IllegalArgumentException("Error: lowerBound cannot be less than or equal to upperBound.");
		} 

		Random r = new Random();
		int value = (r.nextInt(upperBound - lowerBound + 1) + lowerBound);
		logger.debug("lowerBound: {}  upperBound: {}  value: {}", lowerBound, upperBound, value);
		return value;
	}
}
