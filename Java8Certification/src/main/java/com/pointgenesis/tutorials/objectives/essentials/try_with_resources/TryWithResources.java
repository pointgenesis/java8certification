/**
 * Copyright (c) 2016 Point Genesis Solutions LLC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
 * files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN 
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.pointgenesis.tutorials.objectives.essentials.try_with_resources;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Develop code that uses try-with-resources statements, including using classes that implement the AutoCloseable interface
 *
 */
public class TryWithResources {
	/* These are all equivalent assignments:
	private static final Logger logger = LogManager.getLogger(TryWithResources.class.getName());
	private static final Logger logger = LogManager.getLogger(TryWithResources.class);
	private static final Logger logger = LogManager.getLogger();
	Reference: http://logging.apache.org/log4j/2.0/log4j-users-guide.pdf
	Section: 4.1.1.6 Logger Names
	
	I'm using the default, since there is no need for the extra information. The current class is assumed by default.
	*/
	private static final Logger logger = LogManager.getLogger();
	
	public static void readFile(String path) throws IOException {
	    try (BufferedReader br = new BufferedReader(new FileReader(path))) {
	    	int line = 0;
	    	String x = "";
	    	while ((x = br.readLine()) != null) {
	    		line++;
	    		logger.debug("{}", String.format("%08d %s", line, x));
	    	}
	    } 
	}
	

	


}
