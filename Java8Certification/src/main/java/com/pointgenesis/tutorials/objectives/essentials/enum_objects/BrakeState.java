package com.pointgenesis.tutorials.objectives.essentials.enum_objects;

public enum BrakeState {
	NO_PRESSURE, SLIGHT_PRESSURE, FULL_PRESSURE
}
