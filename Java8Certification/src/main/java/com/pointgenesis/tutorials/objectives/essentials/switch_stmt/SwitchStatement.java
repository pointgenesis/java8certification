package com.pointgenesis.tutorials.objectives.essentials.switch_stmt;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.pointgenesis.tutorials.objectives.essentials.enum_objects.BrakeState;
import com.pointgenesis.tutorials.objectives.essentials.enum_objects.TrafficSignal;

/**
 * 
 * Develop code that uses String objects in the switch statement, binary literals, and numeric literals, including underscores in literals
 *
 */
public class SwitchStatement {
	private static final Logger logger = LogManager.getLogger();

	/**
	 * 	//An 8-bit 'byte' value:
		byte aByte = (byte)0b00100001;

		// A 16-bit 'short' value:
		short aShort = (short)0b1010000101000101;

		// Some 32-bit 'int' values:
		int anInt1 = 0b10100001010001011010000101000101;
		int anInt2 = 0b101;
		int anInt3 = 0B101; // The B can be upper or lower case.

		// A 64-bit 'long' value. Note the "L" suffix:
		long aLong = 0b1010000101000101101000010100010110100001010001011010000101000101L;
	 */
	public static int byteSwitch(byte value) {
		logger.debug("value: {}  byteValue: {}", value, (new Integer(value)).byteValue());
		int returnValue;
			switch (value) {
			case (byte)0b00000000:
				returnValue = (int)value;
			case (byte)0b00000001: 
				return 1;
			case (byte)0b00000010: 
				return 2;
			case (byte)0b00000011: 
				return 3;
			case (byte)0b00000100: 
				return 4;
			case (byte)0b00000101:
				return 5;
			case (byte)0b00000110: 
				return 6;
			case (byte)0b00000111:
				return 7;
			case (byte)0b00001000: 
				return 8;
			case (byte)0b00001001:
				return 9;
			case (byte)0b00001010: 
				return 10;
			case (byte)0b00001011:
				return 11;
			case (byte)0b00001100:
				return 12; 
			case (byte)0b00001101:
				return 13;
			case (byte)0b00001110:
				return 14;
			case (byte)0b00001111:
				return 15;
			default:
				return (int)value;
			}
	}
	
	/**
	 * 		// A 16-bit 'short' value:
		short aShort = (short)0b1010000101000101;
	 * @param value
	 * @return
	 */
	public static short shortSwitch(short value) {
		return (short)0;
	}
	
	/**
	 * 	In Java SE 7 and later, any number of underscore characters (_) can appear anywhere between digits in a numerical literal. This feature enables you, for example, to separate groups of digits in numeric literals, which can improve the readability of your code.

		For instance, if your code contains numbers with many digits, you can use an underscore character to separate digits in groups of three, similar to how you would use a punctuation mark like a comma, or a space, as a separator.
		
		The following example shows other ways you can use the underscore in numeric literals:
		
		long creditCardNumber = 1234_5678_9012_3456L;
		long socialSecurityNumber = 999_99_9999L;
		float pi = 	3.14_15F;
		long hexBytes = 0xFF_EC_DE_5E;
		long hexWords = 0xCAFE_BABE;
		long maxLong = 0x7fff_ffff_ffff_ffffL;
		byte nybbles = 0b0010_0101;
		long bytes = 0b11010010_01101001_10010100_10010010;
		You can place underscores only between digits; you cannot place underscores in the following places:
		
		At the beginning or end of a number
		Adjacent to a decimal point in a floating point literal
		Prior to an F or L suffix
		In positions where a string of digits is expected
		The following examples demonstrate valid and invalid underscore placements (which are highlighted) in numeric literals:
		
		float pi1 = 3_.1415F;      // Invalid; cannot put underscores adjacent to a decimal point
		float pi2 = 3._1415F;      // Invalid; cannot put underscores adjacent to a decimal point
		long socialSecurityNumber1
		  = 999_99_9999_L;         // Invalid; cannot put underscores prior to an L suffix
		
		int x1 = _52;              // This is an identifier, not a numeric literal
		int x2 = 5_2;              // OK (decimal literal)
		int x3 = 52_;              // Invalid; cannot put underscores at the end of a literal
		int x4 = 5_______2;        // OK (decimal literal)
		
		int x5 = 0_x52;            // Invalid; cannot put underscores in the 0x radix prefix
		int x6 = 0x_52;            // Invalid; cannot put underscores at the beginning of a number
		int x7 = 0x5_2;            // OK (hexadecimal literal)
		int x8 = 0x52_;            // Invalid; cannot put underscores at the end of a number
		
		int x9 = 0_52;             // OK (octal literal)
		int x10 = 05_2;            // OK (octal literal)
		int x11 = 052_;            // Invalid; cannot put underscores at the end of a number
	 */
	private static void intLiteralSwitch(int numericLiteral) {
		switch(numericLiteral) {
		case 0:
			break;
		case 8:
			break;
		default:
			
		}
	}
	
	public static void main(String[] args) {
		for (TrafficSignal signal : TrafficSignal.values()) {
			enumSwitch(signal);	
		}
	}
	
	private static BrakeState enumSwitch(TrafficSignal signal) {
		System.out.format("You should %s when encountering a %s light.%n", signal.getMeaning(), signal);
		switch(signal) {
		case GREEN:
			return BrakeState.NO_PRESSURE;
		case YELLOW:
			return BrakeState.SLIGHT_PRESSURE;
		case RED:
			return BrakeState.FULL_PRESSURE;
		default:
			throw new IllegalStateException("Traffic light is unexpectedly not working.");
		}
	}
	


}
