package com.pointgenesis.tutorials.objectives.essentials.enum_objects;

public enum TrafficSignal {

	GREEN("GO"), YELLOW("PROCEED WITH CAUTION"), RED("STOP");
	
	private String meaning;
	
	/**
	 * Reference: https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html
	 * 
	 * Note: The constructor of an ENUM must be package-private (as shown below) or private. You cannot declare an ENUM constructor as public or protected.
	 * 
	 * @param meaning the expected action when encountering the state.
	 */
	TrafficSignal(String meaning) {
		this.meaning = meaning;
	}
	
	/**
	 * @return the meaning of the traffic signal state.
	 */
	public String getMeaning() {
		return meaning;
	}
}
