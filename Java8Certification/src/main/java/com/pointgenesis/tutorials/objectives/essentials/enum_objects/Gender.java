package com.pointgenesis.tutorials.objectives.essentials.enum_objects;

public enum Gender {
	MALE("boy/man"),FEMALE("girl/woman");

	private String gender;
	
	Gender(String gender) {
		this.gender = gender;
	}
	
	public String toString() {
		return this.gender;
	}
}
